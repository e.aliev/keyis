'use strict';
document.addEventListener('DOMContentLoaded', () => {
  // let url = 'https://keyis.ru/get_currency_cross/?index=MOEX&country=Russia';

  async function getDate(url) {
    let response = await fetch(url,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        }
      },
    )

    if (response.ok) {
      let json = await response;
      console.log(json)
    } else {
      console.log(response)
    }
  }

  let response = [
    {
      "date": "13/04/2021",
      "open": 3333.3,
      "high": 3542.63,
      "low": 3511.23,
      "close": 3526.3,
      "volume": 0,
      "currency": "RUB"
    },
    {
      "date": "14/04/2021",
      "open": 3567.53,
      "high": 3576.89,
      "low": 3546.86,
      "close": 3576.89,
      "volume": 0,
      "currency": "RUB"
    },
    {
      "date": "12/04/2021",
      "open": 3476.51,
      "high": 3341.7,
      "low": 3472.37,
      "close": 3322.65,
      "volume": 0,
      "currency": "RUB"
    },
    {
      "date": "15/04/2021",
      "open": 3532.13,
      "high": 3578.77,
      "low": 3527.5,
      "close": 3568.51,
      "volume": 0,
      "currency": "RUB"
    },
    {
      "date": "16/04/2021",
      "open": 3578.01,
      "high": 3598.44,
      "low": 3564.97,
      "close": 3598.44,
      "volume": 0,
      "currency": "RUB"
    }
  ],
    url = ''; //хратися слылка api, по умолчанию должна быть ссылка из USD ЦБ

  const currencyDate = [],
    newArr = [];

  function selectMenu() {
    let menuItems = document.querySelectorAll('.currency-value');

    menuItems.forEach(item => {
      addEventListener('click', ev => {
        let target = ev.target;
        ev.preventDefault();
        if (target.hasAttribute('currency-value')) {
          url = target.getAttribute('currency-value');

          //тут должен быть сделать fetch запрос в бд, в виде ссылки кторой будет использовать переменная url, который вернет массив с объектами
          //getDate(url)

          //далее вызввается эта финкция в которую пердается массив пришедший от сервера
          transform(response,currencyDate);
          //-----------------------------------------------------

          //даллее функция которая фармирует график
          cheduling(newArr);
          //---------------------------
        }
      })
    })
  }
  selectMenu()

  //ниже функция которая обрабатывает ответ который пришел от сервера, удаляет ненужные данные и оставляет только те,
  // что нужны для графика. Ниже идет инициализация функции, а поле го вызов, в него как аргумент нужно мередать массив из объектов который пришел в ответе от сервера(это массив response)
  function transform(response,currencyDate) {
    response.forEach((object, b) => {
      let arr = Object.values(object);
      currencyDate.push(arr);
    })

    currencyDate.forEach((obj, i) => {
      obj.splice(5,2)
      obj.forEach((item, i) => {
        if ( typeof item === 'string' && i == 4) {
          obj.unshift(item)
          obj.splice(5, 1)
        }
      })
      newArr.push(obj)
    })
  }
  transform(response,currencyDate);

  function cheduling(newArr) {
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
      let data = google.visualization.arrayToDataTable(currencyDate, true);

      let options = {
        legend:'none',
        bar: { groupWidth: '40%' },
        candlestick: {
          fallingColor: {
            strokeWidth: 0,
            fill: '#a52714'
          },
          risingColor: { strokeWidth: 0, fill: '#0f9d58' }   // green
        },
        animation: {
          duration: 1000,
          easing: 'inAndOut',
          startup: true
        }
      };

      let chart = new google.visualization.CandlestickChart(document.getElementById('chart_div'));

      chart.draw(data, options);
    }
  }
  cheduling(newArr);
})